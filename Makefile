# Makefile

.PHONY = deb arch

PACKAGER = "Evangelos Rigas <erigas@rnd2.org>"
VER = 0.6.1
RND = 83841d3200243fec1e5536dbfeda82f5
SRC = https://gitlab.com/vagnum08/cpupower-gui/uploads/$(RND)/cpupower-gui-$(VER).tar.xz
SRCAR = cpupower-gui_$(VER).orig.tar.xz

fetch-src:
	wget -c $(SRC) -O $(SRCAR)

clean:
	rm *.tar.xz

arch: $(SRCAR)
	cp -f $(SRCAR) ArchLinux/$(SRCAR); \
	cd ArchLinux/; sed -e "s/\@pkgver\@/$(VER)/" PKGBUILD.in  > PKGBUILD; \
	updpkgsums;	makepkg -f; makepkg --allsource

arch-clean:
	cd ArchLinux; rm PKGBUILD $(SRCAR)

$(SRCAR): fetch-src


install-deb-deps:
	sudo apt install zsh dh-systemd autoconf-archive pkg-config build-essential \
	autoconf automake autotools-dev dh-make debhelper devscripts fakeroot xutils \
	lintian pbuilder git packaging-dev

deb: $(SRCAR)
	cp -f $(SRCAR) Debian/$(SRCAR); cd Debian; tar -xvf $(SRCAR); \
	cp -rf debian cpupower-gui-$(VER)/debian; cd cpupower-gui-$(VER); \
	dpkg-buildpackage -F -us -uc -j$(JOBS)
	@echo "============================================================"
	@echo "deb located in Debian"

deb-clean:
	rm -rf Debian/cpupower-gui-$(VER) Debian/$(SRCAR)
